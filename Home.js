/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
} from 'react-native';


const Home =  ({navigation}) => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <Text>Home Screen</Text>
        <Button title={'Go'} onPress={() => navigation.navigate('ScreenTwo')}/>
      </SafeAreaView>
    </>
  );
};



export default Home;
